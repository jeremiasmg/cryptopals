/*

Fixed XOR

Write a function that takes two equal-length buffers and produces their XOR combination.

If your function works properly, then when you feed it the string:

1c0111001f010100061a024b53535009181c

... after hex decoding, and when XOR'd against:

686974207468652062756c6c277320657965

... should produce:

746865206b696420646f6e277420706c6179


*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

char* strXor(const char *str1, const char *str2)
{
	int i, size1, size2, tmpSize;
	char v;
    char *ans; 	
    size1 = (int) strlen(str1);
    size2 = (int) strlen(str1);
    if(size1 != size2) {
        fprintf(stderr, "Size between strings are different!\n");
        exit(EXIT_FAILURE);
    }
    ans =  (char *)malloc(sizeof(char) * size1);
	ans[0] = '\0';
    for (i = 0; i < size1; i++) {
        v = (str1[i] < 58 ? (str1[i] - 48) : (str1[i] - 87)) ^
            (str2[i] < 58 ? (str2[i] - 48) : (str2[i] - 87));
		switch (v){
			case 10: strcat(ans, "a");
			break;
			case 11: strcat(ans, "b");
			break;
			case 12: strcat(ans, "c");
			break;
			case 13: strcat(ans, "d");
			break;
			case 14: strcat(ans, "e");
			break;
			case 15: strcat(ans, "f");
			break;
			default: {
                         tmpSize = strlen(ans);
                         ans[tmpSize] = v + 48;
                         ans[tmpSize + 1] = '\0';
                     }
		}
	}
	return ans;
}
