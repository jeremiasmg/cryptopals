/*

Single-byte XOR cipher

The hex encoded string:

1b37373331363f78151b7f2b783431333d78397828372d363c78373e783a393b3736

... has been XOR'd against a single character. Find the key, decrypt the
message.

You can do this by hand. But don't: write code to do it for you.

How? Devise some method for "scoring" a piece of English plaintext. Character
frequency is a good metric. Evaluate each output and choose the one with the
best score.
Achievement Unlocked

You now have our permission to make "ETAOIN SHRDLU" jokes on Twitter.


*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "allset1.h"

int hexCharInt(char c)
{
    if ((c > 47) && (c < 58)) {
        return (int)(c - 48);
    } else if ((c > 64) && (c < 71)) {
        return (int)(c - 55);
    } else if ((c > 96) && (c < 103)) {
        return (int)(c - 87);
    }

}

unsigned char* decodeHex(const char *str, int *size)
{
    int i, tmp;
    *size = *size / 2;
    unsigned char *decode = (unsigned char *)malloc(sizeof(unsigned char) * (*size));
    for (i = 0; i < *size; i++) {
        tmp = 0;
        tmp = tmp + hexCharInt(str[i * 2]) << 4;
        tmp = tmp + hexCharInt(str[(i * 2) + 1]);
        decode[i] = tmp;
    }
    return decode;
}

unsigned char* singleXOR(unsigned char* str, unsigned char idx, int size)
{
    unsigned char* ret = (unsigned char *)malloc(sizeof(unsigned char) * size);

    for (int i = 0; i < size; i++) {
        ret[i] = str[i] ^ idx;
    }

    return ret;
}

char* decodeSingleByteXOR(const char *str)
{
    double freq[26] = { 8.167, 1.492, 2.782, 4.253, 12.702, 2.228, 2.015, 
                        6.094, 6.966, 0.153, 0.772, 4.025, 2.406, 6.749, 
                        7.507, 1.929, 0.095, 5.987, 6.327, 9.056, 2.758, 
                        0.978, 2.361, 0.150, 1.974, 0.074 };

    int size = strlen(str);
    unsigned char *decode, *xorDecode;
    double score, finalScore = 0;
    int i, j, tmp, id;

    decode = decodeHex(str, &size);
    
    for (i = 0; i < 255; i++) {
        xorDecode = singleXOR(decode, (unsigned char)i, size);
        char *decodeStr = (char *) malloc(sizeof(char) * size);
        memcpy(decodeStr, xorDecode, size);
        score = 0;
        for (j = 0; j < size; j++) {
            if ((decodeStr[j] - 97 > -1) && ((decodeStr[j] - 97) < 26)) {
                score = score + freq[decodeStr[j] - 97];
            } else if ((decodeStr[j] < 31) || (decodeStr[j] > 122)) {
                score = 0;
                break;
            } else {
                score = score - 1.0;
            }
        }

        if (score > finalScore) {
            finalScore = score;
            id = i;
        }

        free(xorDecode);
        free(decodeStr);
    }

    xorDecode = singleXOR(decode, (unsigned char)id, size);
    char *res = (char *) malloc(sizeof(char) * size);
    memcpy(res, xorDecode, size);

    return res;
}
