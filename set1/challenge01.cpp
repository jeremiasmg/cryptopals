/*
Challenge 1

Convert hex to base64

The string:

49276d206b696c6c696e6720796f757220627261696e206c696b65206120706f69736f6e6f7573206d757368726f6f6d

Should produce:

SSdtIGtpbGxpbmcgeW91ciBicmFpbiBsaWtlIGEgcG9pc29ub3VzIG11c2hyb29t
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

const char* convertCharHexToBin(const char c)
{
	switch(c) {
		case '0': return "0000";
		case '1': return "0001";
		case '2': return "0010";
		case '3': return "0011";
		case '4': return "0100";
		case '5': return "0101";
		case '6': return "0110";
		case '7': return "0111";
		case '8': return "1000";
		case '9': return "1001";
		case 'a': return "1010";
		case 'b': return "1011";
		case 'c': return "1100";
		case 'd': return "1101";
		case 'e': return "1110";
		case 'f': return "1111";
	}
	return 0;
}

char *convertBinToBase64(char* s)
{
	int n1,n2;
    char table[65] =
            "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/";
	char *frag64;
	char res[3];

    frag64 = (char *)malloc(sizeof(char) * 3);

    n1 = (s[0] - '0' ? 32 : 0) + (s[1] - '0' ? 16 : 0) + (s[2] - '0' ? 8 : 0) +
         (s[3] - '0' ? 4 : 0) + (s[4] - '0' ? 2 : 0) + (s[5] - '0' ? 1 : 0); 
    n2 = (s[6] - '0' ? 32 : 0) + (s[7] - '0' ? 16 : 0) + (s[8] - '0' ? 8 : 0) +
         (s[9] - '0' ? 4 : 0) + (s[10] - '0' ? 2 : 0) + (s[11] - '0' ? 1 : 0);
	res[0] = table[n1];
	res[1] = table[n2];
	res[2] = '\0';

	strcpy(frag64, res);

	return frag64;
}

char *convertHexBase64(const char *str)
{
	char fragBin[12];
	char *frag64;
	char *result;
	int i, j, size;
	
    size = (int) strlen(str);
    result = (char *)malloc(sizeof(char) * size);

    result[0] = '\0';

    //Mount a binary string with 3 chars in hexa
	for (i = 0; i < size; i = i + 3) {
		fragBin[0] = '\0';
		for (j = i; (j < i + 3); j++) {
			if (j < size){
				strcat(fragBin, convertCharHexToBin(str[j]));
			}
		}
        //Process the fragBin and include in result
		frag64 = convertBinToBase64(fragBin);
		strcat(result, frag64);
		free(frag64);
	}

    return result;
}
