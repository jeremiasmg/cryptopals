#ifndef ALLSET1_H_
#define ALLSET1_H_

//Challenge 01
const char* convertCharHexToBin(const char c);
char* convertBinToBase64(const char *str);
char* convertHexBase64(const char *str);

//Challenge 02
char* strXor(const char *str1, const char *str2);

//Challenge 03
int hexCharInt(char c);
unsigned char* decodeHex(const char *str, int *size);
unsigned char* singleXOR(unsigned char* str, unsigned char idx, int size);
char* decodeSingleByteXOR(const char *str);

//Challenge 04
void challenge04();

#endif
