#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include "set1/allset1.h"

#define MAX 10000

int main()
{

    char *input01, *input02;
    char *answer, *result;

    input01 = (char *) malloc(sizeof(char) * MAX);
    input02 = (char *) malloc(sizeof(char) * MAX);
    answer = (char *) malloc(sizeof(char) * MAX);

    //Challenge 01
    printf("Challenge 01: ");
    strcpy(input01, "49276d206b696c6c696e6720796f757220627261696e206c696b65206120706f69736f6e6f7573206d757368726f6f6d");
    strcpy(answer, "SSdtIGtpbGxpbmcgeW91ciBicmFpbiBsaWtlIGEgcG9pc29ub3VzIG11c2hyb29t");
    result = convertHexBase64(input01);
    assert(strcmp(answer, result) == 0);
    printf("OK\n");

    free(result);

    //Challenge 02
    printf("Challenge 02: ");
    strcpy(input01, "1c0111001f010100061a024b53535009181c");
    strcpy(input02, "686974207468652062756c6c277320657965");
    strcpy(answer, "746865206b696420646f6e277420706c6179");
    result = strXor(input01, input02);
    assert(strcmp(answer, result) == 0);
    printf("OK\n");

    free(result);

    //Challenge 03
    printf("Challenge 03: ");
    strcpy(input01, "1b37373331363f78151b7f2b783431333d78397828372d363c78373e783a393b3736");
    strcpy(answer, "Cooking MC's like a pound of bacon");
    result = decodeSingleByteXOR(input01);
    assert(strcmp(answer, result) == 0);
    printf("OK\n");

    //Challenge 04
    printf("Challenge 04: ");

    challenge04();     

    printf("Not yet!\n");
    free(input01);
    free(input02);
    free(answer);

    return 0;
}
